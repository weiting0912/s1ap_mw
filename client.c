#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/select.h>
#include "s1ap/s1ap_message.h"



void s1ap_buffer_to_OCTET_STRING(
        void *buf, int size, S1AP_TBCD_STRING_t *tbcd_string)
{
    tbcd_string->size = size;
    tbcd_string->buf = core_calloc(tbcd_string->size, sizeof(c_uint8_t));

    memcpy(tbcd_string->buf, buf, size);
}
void s1ap_uint8_to_OCTET_STRING(c_uint8_t uint8, OCTET_STRING_t *octet_string)
{
    octet_string->size = 1;
    octet_string->buf = core_calloc(octet_string->size, sizeof(c_uint8_t));

    octet_string->buf[0] = uint8;
}

void s1ap_uint16_to_OCTET_STRING(c_uint16_t uint16, OCTET_STRING_t *octet_string)
{
    octet_string->size = 2;
    octet_string->buf = core_calloc(octet_string->size, sizeof(c_uint8_t));

    octet_string->buf[0] = uint16 >> 8;
    octet_string->buf[1] = uint16;
}

void s1ap_uint32_to_OCTET_STRING(c_uint32_t uint32, OCTET_STRING_t *octet_string)
{
    octet_string->size = 4;
    octet_string->buf = core_calloc(octet_string->size, sizeof(c_uint8_t));

    octet_string->buf[0] = uint32 >> 24;
    octet_string->buf[1] = uint32 >> 16;
    octet_string->buf[2] = uint32 >> 8;
    octet_string->buf[3] = uint32;
}
void s1ap_uint32_to_ENB_ID(
    S1AP_ENB_ID_PR present, c_uint32_t enb_id, S1AP_ENB_ID_t *eNB_ID)
{
    // d_assert(eNB_ID, return, "Null param");

    eNB_ID->present = present;
    if (present == S1AP_ENB_ID_PR_macroENB_ID)
    {
        BIT_STRING_t *bit_string = &eNB_ID->choice.macroENB_ID;
        // d_assert(bit_string, return, "Null param");

        bit_string->size = 3;
        bit_string->buf = core_calloc(bit_string->size, sizeof(c_uint8_t));

        bit_string->buf[0] = enb_id >> 12;
        bit_string->buf[1] = enb_id >> 4;
        bit_string->buf[2] = (enb_id & 0xf) << 4;

        bit_string->bits_unused = 4;
    } 
    else if (present == S1AP_ENB_ID_PR_homeENB_ID)
    {
        BIT_STRING_t *bit_string = &eNB_ID->choice.homeENB_ID;
        // d_assert(bit_string, return, "Null param");

        bit_string->size = 4;
        bit_string->buf = core_calloc(bit_string->size, sizeof(c_uint8_t));

        bit_string->buf[0] = enb_id >> 20;
        bit_string->buf[1] = enb_id >> 12;
        bit_string->buf[2] = enb_id >> 4;
        bit_string->buf[3] = (enb_id & 0xf) << 4;

        bit_string->bits_unused = 4;
    }
    else
    {
        // d_assert(0, return, "Invalid param");
    }

}
int tests1ap_build_setup_req(
        buffer_t **pkbuf, S1AP_ENB_ID_PR present, c_uint32_t enb_id)
{
    int rv;

    int tac = 12345;
    // plmn_id_t plmn_id;

    S1AP_S1AP_PDU_t pdu;
    S1AP_InitiatingMessage_t *initiatingMessage = NULL;
    S1AP_S1SetupRequest_t *S1SetupRequest = NULL;

    S1AP_S1SetupRequestIEs_t *ie = NULL;
    S1AP_Global_ENB_ID_t *Global_ENB_ID = NULL;
    S1AP_SupportedTAs_t *SupportedTAs = NULL;
    S1AP_SupportedTAs_Item_t *SupportedTAs_Item = NULL;
    S1AP_PLMNidentity_t *PLMNidentity = NULL;
    S1AP_PagingDRX_t *PagingDRX = NULL;

    memset(&pdu, 0, sizeof (S1AP_S1AP_PDU_t));
    pdu.present = S1AP_S1AP_PDU_PR_initiatingMessage;
    pdu.choice.initiatingMessage =
        core_calloc(1, sizeof(S1AP_InitiatingMessage_t));

    initiatingMessage = pdu.choice.initiatingMessage;
    initiatingMessage->procedureCode = S1AP_ProcedureCode_id_S1Setup;
    initiatingMessage->criticality = S1AP_Criticality_reject;
    initiatingMessage->value.present =
        S1AP_InitiatingMessage__value_PR_S1SetupRequest;

    S1SetupRequest = &initiatingMessage->value.choice.S1SetupRequest;

    ie = calloc(1, sizeof(S1AP_S1SetupRequestIEs_t));
    ASN_SEQUENCE_ADD(&S1SetupRequest->protocolIEs, ie);

    ie->id = S1AP_ProtocolIE_ID_id_Global_ENB_ID;
    ie->criticality = S1AP_Criticality_reject;
    ie->value.present = S1AP_S1SetupRequestIEs__value_PR_Global_ENB_ID;

    Global_ENB_ID = &ie->value.choice.Global_ENB_ID;

    ie = calloc(1, sizeof(S1AP_S1SetupRequestIEs_t));
    ASN_SEQUENCE_ADD(&S1SetupRequest->protocolIEs, ie);

    ie->id = S1AP_ProtocolIE_ID_id_SupportedTAs;
    ie->criticality = S1AP_Criticality_reject;
    ie->value.present = S1AP_S1SetupRequestIEs__value_PR_SupportedTAs;

    SupportedTAs = &ie->value.choice.SupportedTAs;

    ie = core_calloc(1, sizeof(S1AP_S1SetupRequestIEs_t));
    ASN_SEQUENCE_ADD(&S1SetupRequest->protocolIEs, ie);
    
    ie->id = S1AP_ProtocolIE_ID_id_DefaultPagingDRX;
    ie->criticality = S1AP_Criticality_ignore;
    ie->value.present = S1AP_S1SetupRequestIEs__value_PR_PagingDRX;

    PagingDRX = &ie->value.choice.PagingDRX;

    // plmn_id_build(&plmn_id, 1, 1, 2);
    c_uint8_t plmnID[3] = {0x00, 0xf1, 0x10};
    

    s1ap_uint32_to_ENB_ID(present, enb_id, &Global_ENB_ID->eNB_ID);
    s1ap_buffer_to_OCTET_STRING(
            plmnID, 3, &Global_ENB_ID->pLMNidentity);

    SupportedTAs_Item = (S1AP_SupportedTAs_Item_t *)
        core_calloc(1, sizeof(S1AP_SupportedTAs_Item_t));
    s1ap_uint16_to_OCTET_STRING(tac, &SupportedTAs_Item->tAC);
    PLMNidentity = (S1AP_PLMNidentity_t *)
        core_calloc(1, sizeof(S1AP_PLMNidentity_t));
    s1ap_buffer_to_OCTET_STRING(
            &plmnID, 3, PLMNidentity);
    ASN_SEQUENCE_ADD(&SupportedTAs_Item->broadcastPLMNs.list, PLMNidentity);

    ASN_SEQUENCE_ADD(&SupportedTAs->list, SupportedTAs_Item);

    *PagingDRX = S1AP_PagingDRX_v64;

    rv = s1ap_encode_pdu(pkbuf, &pdu);
    s1ap_free_pdu(&pdu);

    if (rv != 0)
    {
        fprintf(stderr, "s1ap_encode_pdu() failed\n");
        return -1;
    }

    return 0;
}




// void str_cli(FILE *fp, int sockfd) {
// 	int maxfdp1, stdineof;
// 	fd_set rset;
// 	char sendline[MAXLINE], recvline[MAXLINE];

// 	stdineof = 0;
// 	FD_ZERO(&rset);

// 	while (1) {
// 		if (stdineof == 0) FD_SET(fileno(fp), &rset);
// 		FD_SET(sockfd, &rset);
// 		maxfdp1 = max(fileno(fp), sockfd) + 1;
// 		select(maxfdp1, &rset, NULL, NULL, NULL);
// 		if (FD_ISSET(sockfd, &rset)) {
// 			bzero(recvline, sizeof(recvline));
			// if (read(sockfd, recvline, MAXLINE) == 0) {
			// 	if (stdineof == 1) return;
			// 	else {
			// 		cerr << "str_cli: server terminated prematurely" << endl;
			// 		exit(-1);
			// 	}
			// }
			// fputs(recvline, stdout);
// 		}
// 		if (FD_ISSET(fileno(fp), &rset)) {
// 			bzero(sendline, sizeof(sendline));
// 			if (fgets(sendline, MAXLINE, fp) == NULL) {
// 				stdineof = 1;
// 				shutdown(sockfd, SHUT_WR);
// 				FD_CLR(fileno(fp), &rset);
// 				continue;
// 			}
// 			if (strcmp(sendline, "exit\n") == 0) exit(0);
// 			write(sockfd, sendline, strlen(sendline));
// 		}
// 	}
// }



int main(int argc, const char *argv[])
{
	int sockfd;
	struct sockaddr_in servaddr;
	if (argc != 3)
		fprintf(stderr,"usage: ./echo_client.out <IPaddrss> <PORT>\n");
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		fprintf(stderr,"socket error\n");
		exit(-1);
	}
    

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(atoi(argv[2]));

	if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) < 0) {
		fprintf(stderr,"inet_pton error for %s\n",argv[1]);
		exit(-1);
	}
	
	if (connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
		fprintf(stderr,"connect error\n");
		exit(-1);
	}

	buffer_t* pkbuf = NULL;
    int rv;
    rv = tests1ap_build_setup_req(&pkbuf, S1AP_ENB_ID_PR_macroENB_ID,  0x54f64);
    if (rv != 0) {
        printf("failed");
    }
	write(sockfd, pkbuf->payload, pkbuf->len);
    int n = 0;
    if ((n =read(sockfd, pkbuf->payload, MAX_SDU_LEN))== 0) {
		fprintf(stderr,"server terminated prematurely");
        buffer_free(pkbuf);
		exit(-1);
	}
    else {
        s1ap_message_t msg;
        pkbuf->len = n;

        rv = s1ap_decode_pdu(&msg, pkbuf);
        if(rv == 0){
            printf("success!!!!\n");
        }
        s1ap_free_pdu(&msg);
        buffer_free(pkbuf);
    }
        sleep(10);
    
    
	return 0;
}
