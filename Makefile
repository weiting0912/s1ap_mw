## Process this file with automake to produce Makefile.in

S1AP_PROGRAM_SRCS = \
	client.c \
	$(NULL)

INCLUDES = \
	-I. \
	-I$(S1AP_DIR) \
	-I$(ASN1C_DIR) \
	$(NULL)

CC = gcc
CFLAGS = -g -O2 -Wno-unused-result

S1AP_PROGRAM ?= tests1ap
LIBS = $(S1AP_LIB) $(ASN1C_LIB)
S1AP_LIB = $(S1AP_DIR)/libs1ap.a
ASN1C_LIB = $(ASN1C_DIR)/libasncodec.a
S1AP_DIR=./s1ap
ASN1C_DIR=./s1ap/asn1c

all: $(S1AP_PROGRAM)

$(S1AP_PROGRAM): $(S1AP_LIB) $(ASN1C_LIB) $(S1AP_PROGRAM_SRCS)
	$(CC) $(CFLAGS) $(INCLUDES) $(S1AP_PROGRAM_SRCS) -o $(S1AP_PROGRAM)  $(LDFLAGS) $(LIBS)

$(S1AP_LIB): 
	$(MAKE) -C $(S1AP_DIR)
$(ASN1C_LIB):
	$(MAKE) -C $(ASN1C_DIR)


.PHONY: clean 

clean:
	for dir in $(SUBDIRS); do \
    	$(MAKE) -C $$dir -f Makefile $@; \
 	done
	rm $(S1AP_PROGRAM)

