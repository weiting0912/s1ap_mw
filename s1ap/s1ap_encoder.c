
#include "s1ap_message.h"

int s1ap_encode_pdu(buffer_t **pkbuf, s1ap_message_t *message)
{
    asn_enc_rval_t enc_ret = {0};
    buffer_t* buf;
    // d_assert(message, return CORE_ERROR,);

    if (trace_level >= 5)
        asn_fprint(stdout, &asn_DEF_S1AP_S1AP_PDU, message);


    buf = buffer_alloc();
    *pkbuf = buf;
    // d_assert(*pkbuf, return CORE_ERROR,);

    enc_ret = aper_encode_to_buffer(&asn_DEF_S1AP_S1AP_PDU, NULL,
                    message, (*pkbuf)->payload, MAX_SDU_LEN);
    if (enc_ret.encoded < 0)
    {
        fprintf(stderr, "Failed to encode S1AP-PDU[%ld]\n", enc_ret.encoded);
        buffer_free(*pkbuf);
        return -1;
    }

    (*pkbuf)->len = (enc_ret.encoded >> 3);

    return 0;
}
