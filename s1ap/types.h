#include <string.h>
#include <stdio.h>
#include <stdlib.h> 
#define MAX_SDU_LEN 8192

typedef  char                   c_int8_t;
typedef  unsigned char          c_uint8_t;

typedef  short          c_int16_t;
typedef  unsigned short c_uint16_t;

typedef  int            c_int32_t;
typedef  unsigned int   c_uint32_t;

typedef  long           c_int64_t;
typedef  unsigned long  c_uint64_t;

typedef struct _buffer_t {


    /** this buffer */
    void *payload;

    /** length of this buffer */
    c_uint16_t len; 

} buffer_t;

void buffer_free(buffer_t*);
buffer_t* buffer_alloc();

void *core_calloc(size_t , size_t );