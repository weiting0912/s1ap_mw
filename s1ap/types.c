
#include "types.h"

void buffer_free(buffer_t* buf){
	free(buf->payload);
    free(buf);
	return;
}

buffer_t* buffer_alloc(){
    buffer_t* buf = NULL;
    buf = core_calloc(1, sizeof(buffer_t));
    buf->payload = malloc(MAX_SDU_LEN);
    memset(buf->payload, 0, MAX_SDU_LEN);
    return buf;
}

void *core_calloc(size_t nmemb, size_t size)
{
    void *ptr = NULL;

    ptr = malloc(nmemb * size);
    // d_assert(ptr, return NULL, "nmeb = %d, sizeo = %d", nmemb, size);

    memset(ptr, 0, nmemb * size);
    return ptr;
}