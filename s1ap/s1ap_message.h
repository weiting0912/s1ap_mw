#ifndef _S1AP_MESSAGE_H__
#define _S1AP_MESSAGE_H__

#include "types.h"
#include "s1ap_asn1c.h"

#define trace_level 10

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Octets */
#define S1AP_CLEAR_DATA(__dATA) \
    do { \
        if ((__dATA)->buf) \
        { \
            FREEMEM((__dATA)->buf); \
            (__dATA)->buf = NULL; \
            (__dATA)->size = 0; \
        } \
    } while(0)
#define S1AP_STORE_DATA(__dST, __sRC) \
    do { \
        S1AP_CLEAR_DATA(__dST); \
        (__dST)->size = (__sRC)->size; \
        (__dST)->buf = CALLOC((__dST)->size, sizeof(c_uint8_t)); \
        memcpy((__dST)->buf, (__sRC)->buf, (__dST)->size); \
    } while(0)

typedef struct S1AP_S1AP_PDU s1ap_message_t;

int s1ap_decode_pdu(s1ap_message_t *message, buffer_t *pkbuf);
int s1ap_encode_pdu(buffer_t **pkbuf, s1ap_message_t *message);
int s1ap_free_pdu(s1ap_message_t *message);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif

